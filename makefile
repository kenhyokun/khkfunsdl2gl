INCLUDE := \
	-I./framework/ \
	-I./third_parties/stb/ \

LIB := \
	-L./framework/build/ \
	-lkhkFunSDL2GL \

all:
	g++ -g -Wall -Werror -o main main.cpp $(INCLUDE) $(LIB) -lSDL2 -lGL -lGLEW 
run:
	make
	./main
