/*
License under zlib license
Copyright (C) 2022 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "base_mesh.h"

Transform* create_transform(gl_v3 position, gl_v3 scale){
  Transform *transform = (Transform*)malloc(sizeof(Transform));
  transform->position = position;
  transform->scale = scale;
  transform->model = gl_mat4(1.0f);
  return transform;
}

VERTEX_ARRAY
unsigned int indices[] = {  
			  0, 1, 3, // first triangle
			  1, 2, 3  // second triangle
};

VertexArray* create_vertex_array(){

  VertexArray *vertex_array =
    (VertexArray*)malloc(sizeof(VertexArray));

  return vertex_array;
}

void bind_vertex_array(VertexArray *vertex_array, GLsizeiptr size, float *vertices){
  glGenVertexArrays(1, &vertex_array->vao);
  glGenBuffers(1, &vertex_array->vbo);
  glGenBuffers(1, &vertex_array->ebo);
  glBindVertexArray(vertex_array->vao);

  glBindBuffer(GL_ARRAY_BUFFER, vertex_array->vbo);
  glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertex_array->ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

  // position attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);

  // texture coord attribute
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);

  // glBindBuffer(GL_ARRAY_BUFFER, 0); 
  // glBindVertexArray(0); 
}

void delete_vertex_array(VertexArray *vertex_array){
  glDeleteVertexArrays(1, &vertex_array->vao);
  glDeleteBuffers(1, &vertex_array->vbo);
  glDeleteBuffers(1, &vertex_array->ebo);
}

/*
 Plane
*/
Plane* create_plane(gl_v3 position){
  Plane *plane = (Plane*)malloc(sizeof(Plane));
  plane->vertex_array = create_vertex_array();
  plane->transform = create_transform(position);

  float plane_vertice_arr[] = {
		      // positions          // texture coords
		      0.5f,  0.5f, 0.0f,   1.0f, 1.0f, // top right
		      0.5f, -0.5f, 0.0f,   1.0f, 0.0f, // bottom right
		     -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, // bottom left
		     -0.5f,  0.5f, 0.0f,   0.0f, 1.0f  // top left 
  };

  plane->vertices = plane_vertice_arr;
  bind_vertex_array(plane->vertex_array, sizeof(plane_vertice_arr), plane->vertices);

  return plane;
}

void render_plane(Plane *plane){
  glUseProgram(shader_program);

  set_shader_uniform_v3(shader_program, "light_color", gl_v3(1.0f, .0f, .0f));

  bind_shader(shader_program, plane->vertex_array->vao, plane->transform->model);
  plane->transform->model = gl_mat4(1.0f);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

/*
  Cube
*/
Cube* create_cube(gl_v3 position){
  Cube *cube = (Cube*)malloc(sizeof(Cube));
  cube->vertex_array = create_vertex_array();
  cube->transform = create_transform(position);

  float cube_vertice_arr[] = {
		     -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
		      0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
		      0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		      0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		     -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
		     -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

		     -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		      0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		      0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
		      0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
		     -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
		     -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

		     -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		     -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		     -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		     -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		     -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		     -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

		      0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		      0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		      0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		      0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		      0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		      0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

		     -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		      0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
		      0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		      0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		     -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		     -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

		     -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
		      0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		      0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		      0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		     -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
		     -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
  };

  cube->vertices = cube_vertice_arr;
  bind_vertex_array(cube->vertex_array, sizeof(cube_vertice_arr), cube->vertices);

  return cube;
}

void render_cube(Cube *cube){
  glUseProgram(shader_program);

  set_shader_uniform_v3(shader_program, "light_color", gl_v3(.0f, .0f, 1.0f));

  bind_shader(shader_program, cube->vertex_array->vao, cube->transform->model);
  cube->transform->model = gl_mat4(1.0f);
  glDrawArrays(GL_TRIANGLES, 0, 36);
}
