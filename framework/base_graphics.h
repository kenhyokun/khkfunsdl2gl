/*
License under zlib license
Copyright (C) 2022 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#ifndef BASE_GRAPHICS
#define BASE_GRAPHICS

#include "base_header.h"

#define TEXTURE

struct Texture{
  int width;
  int height;
  int channel_num;
  TEXTURE unsigned char *data;
  TEXTURE unsigned int m_texture;
};

Texture* load_texture(const char *src_file);
void render_texture(Texture *texture);
void clear_texture_data(Texture *texture);

#endif
