/*
License under zlib license
Copyright (C) 2022 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "base_camera.h"

Camera* create_camera(gl_v3 position){
  Camera *camera = (Camera*)malloc(sizeof(Camera));
  camera->position = position;
  camera->direction = gl_v3_0;
  camera->up = gl_v3(0.0f, 1.0f, 0.0f);
  camera->front = gl_v3(0.0f, 0.0f, -1.0f);
  camera->right = gl_v3(1.0f, 0.0f, 0.0f);
  return camera;
}

gl_mat4 get_camera_view(Camera *camera){
  gl_mat4 view = glm::lookAt(
			       camera->position,
			       camera->direction,
			       camera->up
			       );
  return view;
}
