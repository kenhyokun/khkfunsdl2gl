/*
License under zlib license
Copyright (C) 2022 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "base_graphics.h"

#define STB_IMAGE_IMPLEMENTATION
#include<stb_image.h>

Texture* load_texture(const char *src_file){
  Texture *texture = (Texture*)malloc(sizeof(Texture));

  texture->data = stbi_load(src_file,
			    &texture->width,
			    &texture->height,
			    &texture->channel_num,
			    0);

  if(!texture->data){
    cout << "Failed load texture:" << src_file << endl;
    exit(EXIT_FAILURE);
  }

  glGenTextures(1, &texture->m_texture);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texture->m_texture);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  return texture;
}

void render_texture(Texture *texture){
  glTexImage2D(GL_TEXTURE_2D,
	       0,
	       GL_RGB,
	       texture->width,
	       texture->height,
	       0,
	       GL_RGB,
	       GL_UNSIGNED_BYTE,
	       texture->data);

  glGenerateMipmap(GL_TEXTURE_2D);
}

void clear_texture_data(Texture *texture){
  stbi_image_free(texture->data);
}
