#ifndef BASE_HEADER
#define BASE_HEADER

#include<GL/glew.h>
#include<GL/gl.h>
#include<SDL2/SDL.h>
#include<SDL2/SDL_opengl.h>

#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>

#include<iostream>
#include<fstream>
#include<string>

using std::cout;
using std::endl;
using std::ifstream;
using std::string;

typedef glm::vec3 gl_v3;
typedef glm::mat4 gl_mat4;

#define gl_v3_0 (gl_v3){0.0f, 0.0f, 0.0f}

#endif

