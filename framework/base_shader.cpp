/*
License under zlib license
Copyright (C) 2022 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "base_shader.h"

GLuint shader_program;
GLuint shader_light_program;
gl_mat4 shader_view;
gl_mat4 shader_projection;

BASE_SHADER
string load_shader_source(const char *src_file){
  string str = "";
  string line = "";
  ifstream file(src_file);

  if(file.is_open()){
    while(getline(file, line)){
      str += line;
      str += "\n";
    }
    file.close();
  }
  else{
    cout << src_file << ":file not found!" << endl;
    exit(EXIT_FAILURE);
  }

  return str;
}

GLuint create_shader(const char *vertex_shader_src, const char *fragment_shader_src){
  GLuint m_program = glCreateProgram();

  string vs_str = load_shader_source(vertex_shader_src);
  string fs_str = load_shader_source(fragment_shader_src);

  const char *vs_src = vs_str.c_str();
  const char *fs_src = fs_str.c_str();

  GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex_shader, 1, &vs_src, NULL);
  glCompileShader(vertex_shader);

  GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment_shader, 1, &fs_src, NULL);
  glCompileShader(fragment_shader);

  glAttachShader(m_program, vertex_shader);
  glAttachShader(m_program, fragment_shader);
  glLinkProgram (m_program);

  glDeleteShader(vertex_shader);
  glDeleteShader(fragment_shader);

  return m_program;
}

void set_shader_uniform_mat4(GLuint m_program, const char *name, gl_mat4 mat4){
  unsigned int m_uniform = glGetUniformLocation(m_program, name);
  glUniformMatrix4fv(m_uniform, 1, GL_FALSE, &mat4[0][0]);
}

void set_shader_uniform_v3(GLuint m_program, const char *name, gl_v3 v3){
  unsigned int m_uniform = glGetUniformLocation(m_program, name);
  glUniform3fv(m_uniform, 1, &v3[0]); 
}

void bind_shader(GLuint m_program, unsigned int vao, gl_mat4 model){
  glBindVertexArray(vao);
  set_shader_uniform_mat4(m_program, "model", model);
  set_shader_uniform_mat4(m_program, "view", shader_view);
  set_shader_uniform_mat4(m_program, "projection", shader_projection);
}
