/*
License under zlib license
Copyright (C) 2022 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#ifndef BASE_SHADER
#define BASE_SHADER

#include "base_header.h"

extern GLuint shader_program;
extern GLuint shader_light_program;
extern gl_mat4 shader_view;
extern gl_mat4 shader_projection;

GLuint create_shader(const char *vertex_shader_src, const char *fragment_shader_src);
void set_shader_uniform_mat4(GLuint m_program, const char *name, gl_mat4 mat4);
void set_shader_uniform_v3(GLuint m_program, const char *name, gl_v3 v3);
void bind_shader(GLuint m_program, unsigned int vao, gl_mat4 model);

#endif
