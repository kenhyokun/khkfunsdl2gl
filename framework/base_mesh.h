/*
License under zlib license
Copyright (C) 2022 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#ifndef BASE_MESH
#define BASE_MESH

#include "base_header.h"
#include "base_shader.h"

struct Transform{
  gl_v3 position;
  gl_v3 scale;
  gl_mat4 model; // dunno
};

Transform* create_transform(gl_v3 position = gl_v3_0, gl_v3 scale = gl_v3(1.0f, 1.0f, 1.0f));

#define VERTEX_ARRAY

struct VertexArray{
  VERTEX_ARRAY unsigned int vao;
  VERTEX_ARRAY unsigned int vbo;
  VERTEX_ARRAY unsigned int ebo;
};

VertexArray* create_vertex_array();
void bind_vertex_array(VertexArray *vertex_array, GLsizeiptr size, float *vertices);
void delete_vertex_array(VertexArray *vertex_array);

#define PLANE

struct Plane{
  PLANE VertexArray *vertex_array;
  PLANE float *vertices;
  Transform *transform;
};

Plane* create_plane(gl_v3 position = gl_v3_0);
void render_plane(Plane *plane);

#define CUBE

struct Cube{
  CUBE VertexArray *vertex_array;
  CUBE float *vertices;
  Transform *transform;
};

Cube* create_cube(gl_v3 position = gl_v3_0);
void render_cube(Cube *cube);

#endif
