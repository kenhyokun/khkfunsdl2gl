/*
License under zlib license
Copyright (C) 2022 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "base_header.h"
#include "base_shader.h"
#include "base_camera.h"
#include "base_mesh.h"
#include "base_graphics.h"

typedef int32_t i32;
typedef uint32_t u32;

int window_width = 800;
int window_height = 800;

Camera *camera;
Plane *plane;
Cube *cube;
Cube *sample_light;
Texture *wall_texture;
Texture *container_texture;

void start_render(){
  glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glViewport(0, 0, window_width, window_height);

  shader_view = get_camera_view(camera);

  shader_projection =
    glm::perspective(glm::radians(45.0f),
		     (float)window_width / (float)window_height,
		     0.1f, 100.0f);
}

void end_render(){
  shader_view = gl_mat4(1.0f);
  shader_projection = gl_mat4(1.0f);
}

int main(){

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);

  u32 window_flags = SDL_WINDOW_OPENGL;

  SDL_Window *window = SDL_CreateWindow("OpenGL Test",
					SDL_WINDOWPOS_CENTERED,
					SDL_WINDOWPOS_CENTERED,
					window_width,
					window_height,
					window_flags);

  SDL_GLContext context = SDL_GL_CreateContext(window);

  GLenum err = glewInit();
  if (err != GLEW_OK){
    exit(1); 
  }

  shader_program = create_shader("./shaders/transformed.vs", "./shaders/transformed.fs");
  shader_light_program = create_shader("./shaders/light.vs", "./shaders/light.fs");

  camera = create_camera(gl_v3(0.0f, 0.0f, 3.0f));
  plane = create_plane();
  cube = create_cube();
  sample_light = create_cube();

  wall_texture = load_texture("./resources/images/wall.jpg");
  container_texture = load_texture("./resources/images/container.jpg");

  // glUseProgram(shader_program);

  glEnable(GL_DEPTH_TEST);

  bool is_running = true;
  i32 is_full_screen= 0;
  float degree = 0.0f;

  while(is_running){
    SDL_Event Event;
    while (SDL_PollEvent(&Event)){
	if (Event.type == SDL_KEYDOWN){
	  switch (Event.key.keysym.sym){
	    case SDLK_ESCAPE:
	      is_running= 0;
	      break;
	    case 'f':
	      is_full_screen = !is_full_screen;
	      if (is_full_screen){
		SDL_SetWindowFullscreen(window, window_flags | SDL_WINDOW_FULLSCREEN_DESKTOP);
		SDL_GetWindowSize(window, &window_width, &window_height);
		cout << window_width << "," << window_height << endl;
	      }
	      else{
		SDL_SetWindowFullscreen(window, window_flags);
		window_width = 800;
		window_height = 800;
		cout << window_width << "," << window_height << endl;
	      }
	      break;
	  default:
	    break;

	  }
	}
	else if (Event.type == SDL_QUIT){
	  is_running = false;
	}
    }

    // update

    degree += 0.01f;

    camera->position.z = 6.0f;
    camera->position.x = sin(degree) * camera->position.z;
    camera->position.z = cos(degree) * camera->position.z;

    start_render();

    
    render_texture(container_texture);
    cube->transform->model = glm::translate(cube->transform->model, gl_v3(0.1f, 0.2f, 0.5f));
    cube->transform->model = glm::rotate(cube->transform->model, glm::radians(-55.0f), gl_v3(1.0f, 0.0f, 0.0f));
    cube->transform->model = glm::rotate(cube->transform->model, degree, gl_v3(0.0f, 0.0f, 1.0f));
    render_cube(cube);

    cube->transform->model = glm::translate(cube->transform->model, gl_v3(-1.0f, 1.0f, 0.5f));
    cube->transform->model = glm::rotate(cube->transform->model, glm::radians(-55.0f), gl_v3(1.0f, 0.0f, 0.0f));
    cube->transform->model = glm::rotate(cube->transform->model, degree, gl_v3(0.0f, 0.0f, 1.0f));
    render_cube(cube);

    cube->transform->model = glm::translate(cube->transform->model, gl_v3(1.0f, -1.0f, 0.5f));
    cube->transform->model = glm::rotate(cube->transform->model, glm::radians(-55.0f), gl_v3(1.0f, 0.0f, 0.0f));
    cube->transform->model = glm::rotate(cube->transform->model, degree, gl_v3(0.0f, 0.0f, 1.0f));
    render_cube(cube);

    render_texture(wall_texture);
    plane->transform->model = glm::translate(plane->transform->model, gl_v3(0.0f, -0.7f, 0.0f));
    render_plane(plane);

    plane->transform->model = glm::translate(plane->transform->model, gl_v3(0.0f, 0.7f, 0.0f));
    render_plane(plane);

    // render sample light
    glUseProgram(shader_light_program);
    bind_shader(shader_light_program, sample_light->vertex_array->vao, sample_light->transform->model);
    sample_light->transform->model = gl_mat4(1.0f);
    glDrawArrays(GL_TRIANGLES, 0, 36);

    end_render();

    SDL_GL_SwapWindow(window);
  }

  clear_texture_data(wall_texture);
  clear_texture_data(container_texture);
  free(wall_texture);
  free(container_texture);

  delete_vertex_array(plane->vertex_array);
  delete_vertex_array(cube->vertex_array);

  free(camera);
  free(plane);
  free(cube);

  SDL_GL_DeleteContext(context);
  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
}
