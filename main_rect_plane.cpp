/*
License under zlib license
Copyright (C) 2022 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include<GL/glew.h>
#include<GL/gl.h>
#include<SDL2/SDL.h>
#include<SDL2/SDL_opengl.h>

#define STB_IMAGE_IMPLEMENTATION
#include<stb_image.h>

#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>

#include<iostream>
#include<fstream>
#include<string>

using std::cout;
using std::endl;
using std::ifstream;
using std::string;

typedef int32_t i32;
typedef uint32_t u32;

string load_shader_source(const char *src_file){
  string str = "";
  string line = "";
  ifstream file(src_file);

  if(file.is_open()){
    while(getline(file, line)){
      str += line;
      str += "\n";
    }
    file.close();
  }
  else{
    cout << src_file << ":file not found!" << endl;
  }

  return str;
}

int window_width = 800;
int window_height = 800;

int main(){

  string vs_str = load_shader_source("./shaders/transformed.vs");
  string fs_str = load_shader_source("./shaders/transformed.fs");

  const char *vs_src = vs_str.c_str();
  const char *fs_src = fs_str.c_str();

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);

  u32 window_flags = SDL_WINDOW_OPENGL;
  SDL_Window *window = SDL_CreateWindow("OpenGL Test", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_width, window_height, window_flags);
  SDL_GLContext context = SDL_GL_CreateContext(window);

  GLenum err = glewInit();
  if (err != GLEW_OK){
    exit(1); 
  }
  
  i32 is_running = 1;
  i32 is_full_screen= 0;

  GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex_shader, 1, &vs_src, NULL);
  glCompileShader(vertex_shader);

  GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment_shader, 1, &fs_src, NULL);
  glCompileShader(fragment_shader);

  GLuint shader_program;
  shader_program = glCreateProgram();

  glAttachShader(shader_program, vertex_shader);
  glAttachShader(shader_program, fragment_shader);
  glLinkProgram (shader_program);

  glDeleteShader(vertex_shader);
  glDeleteShader(fragment_shader);

  float vertices[] = {
        // positions          // colors           // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right
         0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // bottom left
        -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f  // top left 
  };

  unsigned int indices[] = {  
			    0, 1, 3, // first triangle
			    1, 2, 3  // second triangle
  };

  unsigned int vbo, vao, ebo;
  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);
  glBindVertexArray(vao);

  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

  // position attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);

  // color attribute
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);

  // texture coord attribute
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
  glEnableVertexAttribArray(2);

  // glBindBuffer(GL_ARRAY_BUFFER, 0); 
  // glBindVertexArray(0); 

  // load texture
  unsigned int texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  int width, height, channel_num;
  unsigned char *data = stbi_load("./resources/images/container.jpg", &width, &height, &channel_num, 0);

  if(data){
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
  }
  else{
    cout << "Failed to load texture" << endl;
  }
  stbi_image_free(data);

  glUseProgram(shader_program);
  
  while(is_running){
    SDL_Event Event;
    while (SDL_PollEvent(&Event)){
	if (Event.type == SDL_KEYDOWN){
	  switch (Event.key.keysym.sym){
	    case SDLK_ESCAPE:
	      is_running= 0;
	      break;
	    case 'f':
	      is_full_screen = !is_full_screen;
	      if (is_full_screen){
		SDL_SetWindowFullscreen(window, window_flags | SDL_WINDOW_FULLSCREEN_DESKTOP);
	      }
	      else{
		SDL_SetWindowFullscreen(window, window_flags);
	      }
	      break;
	  default:
	    break;
	  }
	}
	else if (Event.type == SDL_QUIT){
	  is_running = 0;
	}
    }

    // update
    // create transformations
    float ticks = SDL_GetTicks() / 1000;
    glm::mat4 transform = glm::mat4(1.0f);
    transform = glm::translate(transform, glm::vec3(0.5f, -0.5f, 0.0f));
    transform = glm::rotate(transform, ticks, glm::vec3(0.0f, 0.0f, 1.0f));

    glViewport(0, 0, window_width, window_height);
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(shader_program);

    unsigned int transform_uniform = glGetUniformLocation(shader_program, "transform");
    glUniformMatrix4fv(transform_uniform, 1, GL_FALSE, glm::value_ptr(transform));

    // render
    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    SDL_GL_SwapWindow(window);
  }

  glDeleteVertexArrays(1, &vao);
  glDeleteBuffers(1, &vbo);
  glDeleteBuffers(1, &ebo);

  SDL_GL_DeleteContext(context);
  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
}
