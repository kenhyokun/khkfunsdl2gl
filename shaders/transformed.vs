#version 330 core
layout (location = 0) in vec3 vert_pos;
layout (location = 1) in vec2 vert_texture_coord;

out vec2 texture_img;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	gl_Position = projection * view * model * vec4(vert_pos, 1.0);
	texture_img = vec2(vert_texture_coord.x, vert_texture_coord.y);
}