#version 330 core
out vec4 frag_color;

in vec2 texture_img;

uniform sampler2D texture_sampler;
uniform vec3 light_color;

void main()
{
	// frag_color = texture(texture_sampler, texture_img);

	vec3 object_color = texture(texture_sampler, texture_img).xyz;
	frag_color = vec4(object_color * light_color, 1.0);
}