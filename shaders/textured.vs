#version 330 core
layout (location = 0) in vec3 vert_pos;
layout (location = 1) in vec3 vert_color;
layout (location = 2) in vec2 vert_texture_coord;

out vec3 color;
out vec2 texture_img;

void main()
{
	gl_Position = vec4(vert_pos, 1.0);
	color = vert_color;
	texture_img = vert_texture_coord;
}